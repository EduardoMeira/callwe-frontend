window.onload = () => {
    
    const API_BASE = 'https://viacep.com.br/ws';
    const loader = document.querySelector('.loader');
    const cep_message = document.querySelector('.cep-message');
    const cep = document.querySelector('#cep');
    const address = document.querySelector('#address');
    const city = document.querySelector('#city');
    const state = document.querySelector('#state');

    cep.addEventListener('input', function() {
        cep_message.style.color = 'transparent';
        loader.style.display = 'none';
    });

    cep.addEventListener('change', function() {
        clearFileds();
        if (isInvalidCep(this.value)) {
            cep_message.style.color = '#ff0000';
            cep_message.innerHTML = 'CEP inválido';    
            return;        
        }
        cep_message.style.color = 'transparent';
        loader.style.display = 'block';
        loadAddressByCEP(this.value);
    });

    function loadAddressByCEP(cep){
        toggleFields();
        fetch(`${API_BASE}/${cep}/json`)
            .then(response => response.json())
            .then(data => {
                if (data.erro) {
                    toggleFields();
                    loader.style.display = 'none';
                    cep_message.innerHTML = 'CEP não encontrado';
                    cep_message.style.color = '#ff0000';
                    return;
                } 
                loader.style.display = 'none';
                cep_message.style.color = 'transparent';
                address.value = `${data.logradouro}${data.complemento? ` - ${data.complemento}`:''}`;
                city.value = data.localidade;
                state.value = estados.filter(e => e.uf === data.uf)[0].nome;
                toggleFields();
            });
    }

    function isInvalidCep(cep) {
        return cep.replace(/[^0-9-]/g, "").length !== cep.length || cep.replace(/[^0-9]/g, "").length > 8 || cep.length < 8; 
    }

    function toggleFields() {
        address.disabled = !address.disabled;
        city.disabled = !city.disabled;
        state.disabled = !state.disabled;
    }

    function clearFileds() {
        address.value = '';
        city.value = '';
        state.value = '';
    }

    const estados = [
        { uf: 'AC', nome: 'Acre' },
        { uf: 'AL', nome: 'Alagoas' },
        { uf: 'AP', nome: 'Amapá' },
        { uf: 'AM', nome: 'Amazonas' },
        { uf: 'BA', nome: 'Bahia' },
        { uf: 'CE', nome: 'Ceará' },
        { uf: 'DF', nome: 'Distrito Federal' },
        { uf: 'ES', nome: 'Espirito Santo' },
        { uf: 'GO', nome: 'Goiás' },
        { uf: 'MA', nome: 'Maranhão' },
        { uf: 'MS', nome: 'Mato Grosso do Sul' },
        { uf: 'MT', nome: 'Mato Grosso' },
        { uf: 'MG', nome: 'Minas Gerais' },
        { uf: 'PA', nome: 'Pará' },
        { uf: 'PB', nome: 'Paraíba' },
        { uf: 'PR', nome: 'Paraná' },
        { uf: 'PE', nome: 'Pernambuco' },
        { uf: 'PI', nome: 'Piauí' },
        { uf: 'RJ', nome: 'Rio de Janeiro' },
        { uf: 'RN', nome: 'Rio Grande do Norte' },
        { uf: 'RS', nome: 'Rio Grande do Sul' },
        { uf: 'RO', nome: 'Rondônia' },
        { uf: 'RR', nome: 'Roraima' },
        { uf: 'SC', nome: 'Santa Catarina' },
        { uf: 'SP', nome: 'São Paulo' },
        { uf: 'SE', nome: 'Sergipe' },
        { uf: 'TO', nome: 'Tocantins' }
    ]
}